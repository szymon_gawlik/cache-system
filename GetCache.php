<?php
declare(strict_types = 1);
namespace Cache;
use Cache\SetCache;

abstract class GetCacheAbs extends SetCache
{
    public abstract static function getObject(string $name) : \SimpleXMLElement;

    public abstract static function getArray(string $name) : array;

    public abstract static function fileExists(string $name) : bool;

    protected abstract function _toArray(\SimpleXMLElement $obj) : array;

    public abstract static function getBy(string $name, array $params) : \stdClass;
}

class GetCache extends GetCacheAbs
{
    public static function getObject(string $name) : \SimpleXMLElement
    {
        // TODO: Implement getObject() method.
        $content  = simplexml_load_file(self::setPath($name));
        if(!$content->type) throw new \Error("Brak zdefiniowanego typu! Usuń cache i spróbuj ponownie");
        if($content->type != "object") throw new \Error("getObject have to return an object!");

        return $content;
    }

    public static function getArray(string $name) : array
    {
        // TODO: Implement getArray() method.
        $content  = simplexml_load_file(self::setPath($name));
        if(!$content->type) throw new \Error("Brak zdefiniowanego typu! Usuń cache i spróbuj ponownie");
        if($content->type != 'array') throw new \Error("getArray have to return an array!");

        return static::_toArray($content);
    }

    public static function fileExists(string $name) : bool
    {
        // TODO: Implement _fileExists() method.
        $url = self::setPath($name);
        if(file_exists($url)) {
            $content  = simplexml_load_file($url);
            return ($content->expire > time()) ? false : true;
        } else return true;
    }

    protected function _toArray(\SimpleXMLElement $obj) : array
    {
        // TODO: Implement _toArray() method.
        $array = [];
        foreach ($obj as $key => $item) {
            $array[$key] = gettext($item);
        }

        return $array;
    }

    public static function getBy(string $name, array $params) : \stdClass
    {
        // TODO: Implement getBy() method.
        $url = self::setPath($name);
        if(count($params) !== 1) throw new \Exception('This version don`t support multiply arguments!');
        if(!file_exists($url)) throw new \Exception('File doesn`t exist!');

        $content = simplexml_load_file($url);
        $result = (object) null;

        foreach ($content->item as $item) {
            if($item->{key($params)} == $params[key($params)]) {
                $result->item[] = $item;
            }
        }

        if(count($result->item) === 0) throw new \Error("Brak wyników!");

        return $result;
    }
}