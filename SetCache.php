<?php
declare(strict_types = 1);

namespace Cache;

abstract class OpenFile
{
    public static function openFile(string $name)
    {
        $xml      = new \DOMDocument();
        $xml      -> loadXML("<$name/>");
        return $xml;
    }

    public static function setPath(string $name)
    {
        $hashName = md5($name);
        return  __DIR__."/../../web/cache/$hashName.xml";
    }
}

class SetCache extends OpenFile
{
    public static function setArray(array $data, string $name, int $expire)
    {
        $xml      = self::openFile($name);
        $exp      = $xml->createElement('expire',(string)(time()+$expire));
        $xml      ->documentElement->appendChild($exp);

        $xml->documentElement->appendChild($xml->createElement('type','array'));

        while ($value = current($data)) {
            $name = (gettype(key($data)) === 'integer') ? 'item' : (string)key($data);
            $xml->documentElement->appendChild($xml->createElement($name,(string)$value));
            next($data);
        }

        $xml->formatOutput = true;
        $save = $xml->save(self::setPath($name));
        if($save == false) {
            throw new \Error('Zapis do pliku nie powiódł się!');
        }

        return;
    }

    public static function setObject(\stdClass $data, string $name, int $expire)
    {
        // TODO: Implement set() method.
        $xml      = self::openFile($name);
        $exp      = $xml->createElement('expire',(string)(time()+$expire));
        $xml      ->documentElement->appendChild($exp);

        $xml->documentElement->appendChild($xml->createElement('type','object'));
        foreach ($data->item as $value) {
            $item = $xml->createElement('item');
            foreach ($value as $key => $val) {
                $item->appendChild($xml->createElement((string)$key, (string)$val));
            }
            $xml->documentElement->appendChild($item);
        }

        $xml->formatOutput = true;
        $save = $xml->save(self::setPath($name));
        if($save == false) {
            throw new \Error('Zapis do pliku nie powiódł się!');
        }
        return;
    }
}